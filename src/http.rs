use reqwest::{Client, StatusCode};

pub fn get(domain: &str, path: &str) -> Result<String, ()> {
    let client = Client::new();
    let url = format!("https://{}/{}", domain, path);

    let mut response = client.get(&url).send();

    match &mut response {
        Ok(response) if response.status() == StatusCode::OK
            => Ok(response.text().unwrap()),
        Ok(response) => {
            println!("{}", response.status());
            Err(())
        },
        Err(e) => {
            eprintln!("Error during GET! {}", e);
            Err(())
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn get() {
        let domain = "example.com";
        let path = "/";

        let body = super::get(domain, path);
        assert!(body.is_ok());

        println!("{:?}", body);
    }
}


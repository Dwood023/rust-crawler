use rocket::State;
use std::sync::{Mutex, Arc};

use crate::{
    Map,
    index::Index,
    crawler
};

struct AppState {
    indexed: Arc<Mutex<Map<String, Index>>>
}

#[get("/crawl/<domain>")]
fn crawl(domain: String, state: State<AppState>) -> String {

    let found = crawler::crawl(&domain);

    state.indexed.lock().unwrap().insert(domain, found);

    format!("Index after crawling: {:?}", state.indexed)
}
#[get("/count/<domain>")]
fn count(domain: String, state: State<AppState>) -> String {
    let indexed = state.indexed.lock().unwrap();

    match indexed.get(&domain) {
        Some(index) => index.count().to_string(),
        _ => "0".to_string()
    }
}
#[get("/urls/<domain>")]
fn urls(domain: String, state: State<AppState>) -> String {
    let indexed = state.indexed.lock().unwrap();

    match indexed.get(&domain) {
        Some(index) => index.to_string(),
        _ => "0".to_string()
    }
}

pub fn server() -> rocket::Rocket {
    rocket::ignite()
        .manage(AppState {
            indexed: Arc::new(Mutex::new(Map::new()))
        })
        .mount("/", routes![crawl, count, urls])
}
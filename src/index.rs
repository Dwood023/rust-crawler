use std::collections::HashSet;
use std::fmt;

#[derive(Debug, Eq, PartialEq)]
pub struct Index {
    entries: HashSet<String>
}
impl Index {
    pub fn new() -> Index {
        Index {
            entries: HashSet::new()
        }
    }
    pub fn add(&mut self, path: &str) {
        self.entries.insert(path.to_owned());
    }
    pub fn has(&self, entry: &str) -> bool {
        self.entries.contains(entry)
    }
    pub fn count(&self) -> usize {
        self.entries.iter().count()
    }
}

impl fmt::Display for Index {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s = self.entries.iter()
            .map(|s| &**s)
            .collect::<Vec<&str>>()
            .join(", ");

        write!(f, "{}", s)
    }
}
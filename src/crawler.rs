use std::sync::{Arc, RwLock};
use std::thread;

use crate::{
    index::Index,
    parser::{extract_links, is_local_link},
    http::get
};

/// Allows threads to share findings on what links exist,
/// which have already been indexed, 
/// and which have already been found to be broken
type SharedIndex<'a> = Arc<RwLock<Index>>;

/// Crawl domain, following links recursively
pub fn crawl(domain: &str) -> Index {

    let found = Index::new();
    let blacklist = Index::new();

    let f = Arc::new(RwLock::new(found));
    let b = Arc::new(RwLock::new(blacklist));

    crawl_recursively(f.clone(),b,domain,"/");

    Arc::try_unwrap(f).unwrap().into_inner().unwrap()
}

/// Crawls path on domain, adding it to index if response 200
/// Then parses body for valid, local links
/// 
/// Spawns a worker thread for each link, which calls this function again
/// This function waits for all spawned processes to re-join
/// Workers can communicate their findings (valid and invalid links)
/// via their SharedIndex's (Arc<RwLock>)
fn crawl_recursively(f: SharedIndex, b: SharedIndex, domain: &str, link: &str) {

    let mut handles = vec!();
    if let Ok(body) = get(domain, link) {
        println!("Added {} to found!", link);
        f.write().unwrap().add(link);

        for link in extract_links(&body) {

            let found = f.clone();
            let blacklist = b.clone();
            let domain = domain.to_string();

            let handle = thread::spawn(move || {
                if !is_local_link(&link) {
                    println!("{} is not local, discarding.", link);
                } else if blacklist.read().unwrap().has(&link) {
                    println!("{} is blacklisted, moving on.", link);
                } else if found.read().unwrap().has(&link) {
                    println!("{} is already indexed, skipping.", link);
                }  else {
                    crawl_recursively(found, blacklist, &domain, &link);
                }
            });

            handles.push(handle);
        }
        for handle in handles {
            handle.join();
        }
    } else {
        b.write().unwrap().add(link);
        println!("Added {} to blacklist!", link);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn crawl() {

        let mut expected = Index::new();
        expected.add("/");
        expected.add("/game?title=CS:GO");
        expected.add("/organise");

        let found = super::crawl("app.tournee.now.sh");

        assert_eq!(expected, found);
    }
}


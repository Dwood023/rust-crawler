#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use] extern crate lazy_static;
#[macro_use] extern crate rocket;

mod crawler;
mod index;
mod parser;
mod http;
mod server;

use std::collections::{HashMap, HashSet};

pub type Path = String;
pub type Domain = String;
pub type Set<T> = HashSet<T>;
pub type Map<K, V> = HashMap<K, V>;

fn main() {
    server::server().launch();
}